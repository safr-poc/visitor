# build environment
FROM node:12.2.0-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
COPY .env.production /app/.env
RUN npm install
#RUN npm install react-scripts@3.0.1 -g 
COPY . /app
RUN npm run build

# production environment
FROM bitnami/nginx:1.16.1
COPY --from=build /app/build /app
COPY nginx/my_server_block.conf /opt/bitnami/nginx/conf/server_blocks/my_server_block.conf
RUN ls -alh /opt/bitnami/nginx/conf/server_blocks/
# EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]