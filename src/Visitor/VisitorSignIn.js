import React, { useState, useEffect, Component } from 'react';
import { Box, Grid, Button, Typography } from '@material-ui/core';
import useWindowSize from '../Utils/useWindowSize';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function VisitorSignIn() {
    const size = useWindowSize();
    return (
        <Box display="flex" alignItems="center" justifyContent="center" style={{ height: size.height, width: size.width }}>
            <Grid container justify="center" spacing={2} style={{height:'500px'}}>
                <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
                <Grid item xs={8} style={{ padding: '10px' }}>
                    <Link to="/" variant="body2" style={{ color: 'black', textDecoration: 'none' }}>Back</Link>
                    <div>&nbsp;</div>
                    <Grid container direction='column'>
                        <Grid item style={{ padding: '10px', textAlign: 'center' }}>
                            <Link to="/Visitor/New/Camera" variant="body2" style={{ color: 'white', textDecoration: 'none' }}>
                                <Button variant="contained" color="primary" size="large" fullWidth >New Visitor Registration</Button>
                            </Link>
                        </Grid>
                        <Grid item style={{ padding: '10px', textAlign: 'center' }}>
                            <Link to="/Visitor/Returning" variant="body2" style={{ color: 'white', textDecoration: 'none' }}>
                                <Button variant="contained" color="primary" size="large" fullWidth >Returning Visitor</Button>
                            </Link>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
                <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
                <Grid item xs={8} style={{ padding: '10px' }}>
                    
                </Grid>
                <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
            </Grid>
        </Box>
    );
}

export default VisitorSignIn;
