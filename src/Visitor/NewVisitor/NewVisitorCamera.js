import React, { useState, useEffect, Component } from 'react';
import { Box, Grid, Button,TextField, Typography,Snackbar } from '@material-ui/core';
import useWindowSize from '../../Utils/useWindowSize';
import { BrowserRouter as Router, Route, Link,withRouter} from "react-router-dom";
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import axios from 'axios';

function NewVisitorCamera() {
  const size = useWindowSize();
  const [dataUri,onTakePhoto]=useState('');
  const [isFinished,setFinished]=useState(false);
  const [postResult,setPostResult] = useState('');
  const [retakeButtonEnable,setRetakeButtonEnable]=useState(true);
  const [nextButtonEnable,setNextButtonEnable]=useState(true);
  const [alertOpen,setAlertOpen]=useState(false);
  const [peopleId,setPeopleId]=useState('');
  const [waiting,IsWaiting]=useState(false);
  const [oldGuest,setOldGuest]=useState('');
  function retakePhoto(){
    setFinished(false);
  }
  function handleAlertClose(){
    setAlertOpen(false);
  }
  useEffect(()=>{
    if(dataUri!=''){
      IsWaiting(true);
      var url = process.env.REACT_APP_API_URL;
      axios.post(url+'/safr/person/image/save',{
        "Base64Image":dataUri,
        "Directory": process.env.REACT_APP_DIRECTORY 
      })
      .then(function (response) {
        IsWaiting(false);
        if (!response.data.isOk){
          setPostResult(response.data.errorMessage);
          setRetakeButtonEnable(false);
          setNextButtonEnable(true);
          setAlertOpen(true);
        }else{
          if (response.data.lastOccurance!=0)
          {
            setOldGuest(response.data.name);
            setPeopleId(response.data.personId);
            setRetakeButtonEnable(true);
            setNextButtonEnable(false);
          }
          else (response.data.peopleId=='00000000-0000-0000-0000-000000000000')
          {
            setRetakeButtonEnable(true);
            setNextButtonEnable(false);
            return;
          }
        }
        setFinished(true);
      })
      .catch(function (error) {
        IsWaiting(false);
        setFinished(true);
      });
      setFinished(true);
    }
  },[dataUri]);
  useEffect(()=>{
    setFinished(false);
  },[]);
  return (
    <Box display="flex" alignItems="center" justifyContent="center" style={{ height: size.height, width: size.width }}>
      <Grid container justify="center" spacing={2}>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
        <Grid item xs={8} style={{ padding: '10px' }}>
          <Link to="/Visitor/" variant="body2" style={{ color: 'black', textDecoration: 'none' }}>Back</Link>
          <div>&nbsp;</div>
          {!isFinished && (
            <Camera imageType={IMAGE_TYPES.PNG} onTakePhoto = { (dataUri) => { onTakePhoto(dataUri);  }} />
          )}
          {isFinished && ([
            <img src={dataUri} style={{height:'400px'}} />,
            <br/>,
            <Button disabled={retakeButtonEnable}  variant="contained" color="primary" style={{marginRight:'10px'}} onClick={retakePhoto}>Retake</Button>,
            <Link to={'/Visitor/New/'+peopleId} style={{ color: 'white', textDecoration: 'none',paddingRight:'10px' }}>
              <Button disabled={nextButtonEnable}  variant="contained" color="primary">
                Next
              </Button> 
            </Link>,
            (waiting && "Please Wait"),
            (oldGuest && "Hi, Welcome Back "+oldGuest)
          ])}
        </Grid>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
      </Grid>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={alertOpen}
        onClose={handleAlertClose}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id">{postResult}</span>}
      />
    </Box>
  );
}

export default NewVisitorCamera;
