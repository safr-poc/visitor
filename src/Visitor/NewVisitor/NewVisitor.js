import React, { useState, useEffect, Component } from 'react';
import { Box, Grid, Button, TextField,FormControl, Hidden,Input, Divider } from '@material-ui/core';
import useWindowSize from '../../Utils/useWindowSize';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import useForm from 'react-hook-form'
import axios from 'axios';

const NewVisitor = ({match}) => {
  const size = useWindowSize();
  const { register, handleSubmit, errors,watch } = useForm()
  const [submitDisabled, setSubmitDisabled] = useState(false);
  const [nextDisabled, setNextDisabled] = useState(true);
  const onSubmit = person => { 
    setSubmitDisabled(true);
    setNextDisabled(true);
    const formsData = JSON.stringify(person);
    var url = process.env.REACT_APP_API_URL;
    axios.post(url+'/safr/person/save',{
      person
      ,directory : person.directory
    })
    .then(function (response) {
      alert('success');
      setSubmitDisabled(true);
      setNextDisabled(false);
    })
    .catch(function (error) {
      alert(error);
      setNextDisabled(true);
      setSubmitDisabled(false);
    });
  }
  return (
    <Box display="flex" alignItems="center" justifyContent="center" style={{ height: size.height, width: size.width }}>
      <Grid container justify="center" spacing={2}>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
        <Grid item xs={8} style={{ padding: '10px' }}>
          <Link to="/Visitor/New/Camera" variant="body2" style={{ color: 'black', textDecoration: 'none' }}>Back</Link>
          <form onSubmit={handleSubmit(onSubmit)}>
          {/* <FormControl margin="normal" required fullWidth> */}
            <Input inputRef={register} type='hidden' value="reski" name='directory'/>
            <Input inputRef={register} type='hidden' value={match.params.p} name='MergedWith'/>
            <TextField inputRef={register({ required: true, maxLength: {value:50,message:'Maximum Length Size'} })} fullWidth id="outlined-name" name="name" label="Name" margin="normal" variant="outlined"/>
            {errors.name && errors.name.type === 'required' && 'Your input is required'}
            {errors.name && errors.name.message} 
            <TextField inputRef={register({ required: true })} fullWidth id="outlined-organization" name="homeLocation" label="Organization" margin="normal" variant="outlined"/>
            {errors.homeLocation && errors.homeLocation.type === 'required' && 'Your input is required'}
            {errors.homeLocation && errors.homeLocation.message} 
            <TextField  inputRef={register({ required: true })} fullWidth id="outlined-phone" name="validationPhone" label="Phone" margin="normal" variant="outlined"  />
            {errors.validationPhone && errors.validationPhone.type === 'required' && 'Your input is required'}
            {errors.validationPhone && errors.validationPhone.message} 
            <TextField  inputRef={register({ required: true })} fullWidth id="outlined-email" name="validationEmail" label="Email" margin="normal" variant="outlined" />
            {errors.validationEmail && errors.validationEmail.type === 'required' && 'Your input is required'}
            {errors.validationEmail && errors.validationEmail.message} 
            <TextField  inputRef={register({ required: true })} fullWidth id="outlined-objective" name="tag" label="Objective" margin="normal" variant="outlined" multiline rowsMax="4"  />
            {errors.tag && errors.tag.type === 'required' && 'Your input is required'}
            {errors.tag && errors.tag.message} 
            <Box>
              <Button disabled={submitDisabled} variant="contained" color="primary" type='submit' value='submit'>Register</Button>
              &nbsp;
              <Link to={'/Visitor/New/'+match.params.p+'/MeetPeople/'} style={{ color: 'white', textDecoration: 'none' }}>
                <Button disabled={nextDisabled} variant="contained" color="primary">Next</Button>
              </Link>
            </Box>
            {/* </FormControl> */}
          </form>
        </Grid>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
        <Grid item xs={8} style={{ padding: '10px' }}>
          
        </Grid>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
      </Grid>
    </Box>
  );
}

export default NewVisitor;
