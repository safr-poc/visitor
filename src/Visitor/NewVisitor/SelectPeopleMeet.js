import React, { useState, useEffect, Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid, Button, TextField, InputBase, Paper,Divider,IconButton } from '@material-ui/core';
import useWindowSize from '../../Utils/useWindowSize';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

const SelectPeopleToMeet = ({match}) => {
  const classes = useStyles();
  const size = useWindowSize();
  return (
    <Box display="flex" alignItems="center" justifyContent="center" style={{ height: size.height, width: size.width }}>
      <Grid container justify="center" spacing={2} style={{height:'500px'}}>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
        <Grid item xs={8} style={{ padding: '10px' }}>
          <Link to={"/Visitor/New/"+match.params.p} variant="body2" style={{ color: 'black', textDecoration: 'none' }}>Back</Link>
          <Box display="flex">&nbsp;</Box>
          <form noValidate autoComplete="off">
            <Paper className={classes.root}>
              <InputBase placeholder="Search People" className={classes.input}  inputProps={{ 'aria-label': 'search people' }} />
              <Divider orientation="vertical" className={classes.divider} />
              <IconButton color="primary" className={classes.iconButton} aria-label="directions">
                <SearchIcon />
              </IconButton>
            </Paper>
          </form>
          <Box display='flex' style={{marginTop:'10px'}}>
            <Button variant="contained" color="primary">
              <Link to='/' style={{ color: 'white', textDecoration: 'none' }}>Submit</Link>
            </Button>
          </Box>
        </Grid>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
      </Grid>
    </Box>
  );
}

export default SelectPeopleToMeet;
