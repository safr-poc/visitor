import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import React from 'react';
import NewVisitor from './Visitor/NewVisitor/NewVisitor';
import NewVisitorCamera from './Visitor/NewVisitor/NewVisitorCamera';
import Home from './Home';
import VisitorSignIn from "./Visitor/VisitorSignIn";
import SelectPeopleFromCamera from './Visitor/ReturningVisitor/SelectPeopleFromCamera';
import SelectPeopleMeet from './Visitor/NewVisitor/SelectPeopleMeet';
function RouterNavigation() {
    return (
        <Router>
            <Route path="/" exact component={Home} />
            <Route path="/Visitor/" exact component={VisitorSignIn} />
            <Route path="/Visitor/New/Camera" exact component={NewVisitorCamera} />
            <Route path="/Visitor/New/:p/MeetPeople" exact component={SelectPeopleMeet} />
            <Route path="/Visitor/Returning" exact component={SelectPeopleFromCamera} />
            <Route path="/Visitor/New/:p" exact component={NewVisitor} />
        </Router>
    );
}
export default RouterNavigation;