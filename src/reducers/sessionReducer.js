import * as actionTypes from '../actions';

const initialState = {
  loggedIn: true,
  user: {
    first_name: 'Reski',
    last_name: 'Rukmantiyo',
    email: 'demo@devias.io',
    avatar: '/images/avatars/avatar_gundam.jpg',
    bio: 'Organization Admin',
    role: 'ADMIN' // ['GUEST', 'USER', 'ADMIN']
  },
  data : {
    vcloudAuth : ''
  }
};

const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SESSION_LOGIN: {
      return {
        ...initialState
      };
    }

    case actionTypes.SESSION_LOGOUT: {
      return {
        ...state,
        loggedIn: false,
        user: {
          role: 'GUEST'
        }
      };
    }

    case actionTypes.VCLOUD_LOGIN: {
      return {
        ...state,
        data : {
          vcloudAuth : action.session.data.vcloudAuth
        }
      };
    }

    default: {
      return state;
    }
  }
};

export default sessionReducer;
