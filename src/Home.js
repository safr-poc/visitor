import React, { useState, useEffect, Component } from 'react';
import { Box, Grid, Button, Typography } from '@material-ui/core';
import useWindowSize from './Utils/useWindowSize';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function Home() {
  const size = useWindowSize();
  return (
    <Box display="flex" alignItems="center" justifyContent="center" style={{ height: size.height, width: size.width }}>
      <Grid container justify="center" spacing={2}>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
        <Grid item xs={8} style={{ padding: '10px' }} alignContent='center' alignItems='center'>
          <Typography align='center' variant="h3" component="h2">Welcome to Our Buildings</Typography>
        </Grid>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
        <Grid item xs={8} style={{ padding: '10px' }}>
          <Grid container direction='column'>
            <Grid item style={{ padding: '10px', textAlign: 'center' }}>
              <Link to="/Visitor/" variant="body2" style={{ color: 'white', textDecoration: 'none' }}>
                <Button variant="contained" color="primary" size="large" fullWidth >Visitor Sign In</Button>
              </Link>
            </Grid>
            <Grid item style={{ padding: '10px', textAlign: 'center' }}>
              <Button variant="contained" color="primary" size="large" fullWidth >
                Delivery
              </Button>
            </Grid>
            <Grid item style={{ padding: '10px', textAlign: 'center' }}>
              <Button variant="contained" color="primary" size="large" fullWidth >
                Scan
             </Button>
            </Grid>
            <Grid item style={{ padding: '10px', textAlign: 'center' }}>
              <Button variant="contained" color="primary" size="large" fullWidth >
                Request for Assistance
            </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={2} style={{ padding: '10px' }} >&nbsp;</Grid>
      </Grid>
    </Box>
  );
}

export default Home;
