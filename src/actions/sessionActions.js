export const SESSION_LOGIN = 'SESSION_LOGIN';
export const SESSION_LOGOUT = 'SESSION_LOGOUT';
export const VCLOUD_LOGIN = 'VCLOUD_LOGIN';

export const login = () => dispatch =>
  dispatch({
    type: SESSION_LOGIN
  });

export const logout = () => dispatch =>
  dispatch({
    type: SESSION_LOGOUT
  });

  export const vCloudLogin = () => dispatch =>
  dispatch({
    type: VCLOUD_LOGIN
  });
