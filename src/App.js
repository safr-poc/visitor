
import React from 'react';
import { ThemeProvider } from '@material-ui/styles';
import useWindowSize from './Utils/useWindowSize';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import RouterNavigation from './RouterNavigation';
import { PersistGate } from 'redux-persist/integration/react'
import { configureStore } from './store';
import { Provider as StoreProvider } from 'react-redux';

const dotenv = require('dotenv').config();
const { store, persistor } = configureStore();
const theme = {
  background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
};

function App() {
  const screen = useWindowSize();
  return (
    <StoreProvider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider>
          <RouterNavigation />
        </ThemeProvider>
      </PersistGate>
    </StoreProvider>
  );
}

export default App;
